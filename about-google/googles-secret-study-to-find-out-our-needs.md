Google attempts to find out the needs of the search users by examining the logs to see what kind of queries yielded unsatisfied results

**DIN** (**Daily Information Needs**) - Study project that surveys search users to find out what users really _want to know_

Google will ask the DIN study subjects 8 times a day what the subject wants answered at that very moment
- "What is the weather like"
- "How is traffic"
- "Whats the score of the game"

Google used this data to create 21 categories based on the questions the DIN subjects had
- Most people wanted to know public transit information so Google focused on improving how transit data is presented to users based on transit related queries
 - Train times
 - Traffic times
 - Is train on time today


Some DIN subjects were shift workers who would want to know when their next work shift was
- Google worked to add new features to help shift workers find out when their next work shift was
